<?php 
/*----------------------------------------------------------------*\

	Template Name: Confirmation
	
\*----------------------------------------------------------------*/
?>

<?php get_header(); ?>

<?php get_template_part('template-parts/elements/navigation'); ?>

<?php get_template_part('template-parts/sections/post-header'); ?>

<main id="main-content">
	<?php if ( get_field('message') ) : ?>
		<article>
			<section class="is-narrow">
				<p><?php the_field('message'); ?></p>
			</section>
		</article>
		<?php $posts = get_field('previews'); ?>
	<?php else : ?>
		<article>
			<section>
				<h2>Uh Oh. Something is missing.</h2>
				<p>Looks like this page has no content.</p>
			</section>
		</article>
	<?php endif; ?>
</main>

<?php get_template_part('template-parts/sections/post-footer'); ?>

<?php get_footer(); ?>