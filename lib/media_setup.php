<?php
/*----------------------------------------------------------------*\
	ADDITIONAL IMAGE SIZES
\*----------------------------------------------------------------*/
add_image_size( 'placeholder', 20, 20 ); 
function placeholder_image_sizes($sizes) {
	$sizes['placeholder'] = __( 'Placeholder' );
	return $sizes;
}
add_filter('image_size_names_choose', 'placeholder_image_sizes');
add_image_size( 'small', 400, 400 ); 
function small_image_sizes($sizes) {
	$sizes['small'] = __( 'Small' );
	return $sizes;
}
add_filter('image_size_names_choose', 'small_image_sizes');
add_image_size( 'xlarge', 2000, 2000 ); 
function large_image_sizes($sizes) {
	$sizes['xlarge'] = __( 'X-Large' );
	return $sizes;
}
add_filter('image_size_names_choose', 'large_image_sizes');

/*----------------------------------------------------------------*\
    ADDITIONAL FILE FORMATS
\*----------------------------------------------------------------*/
function cc_mime_types($mimes) {
	$mimes['svg'] = 'image/svg+xml';
	$mimes['zip'] = 'application/zip';
	return $mimes;
 }
 add_filter('upload_mimes', 'cc_mime_types');

/*----------------------------------------------------------------*\
	LAZY LOAD CLASSES FOR WYSIWYG
\*----------------------------------------------------------------*/
add_filter('acf_the_content', 'filter');
function filter($content) {
    return str_replace('src="', 'data-expand="-250" data-src="', $content);
}

function add_image_class($class){
	$class .= ' lazyload blur-up';
	return $class;
}
add_filter('get_image_tag_class','add_image_class');