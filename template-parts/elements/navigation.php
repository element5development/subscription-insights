<?php 
/*----------------------------------------------------------------*\

	PRIMARY NAVIGATION

\*----------------------------------------------------------------*/
?>
<div class="primary-navigation">
	<nav>
		<a href="<?php echo get_home_url(); ?>">LOGO</a>
		<?php wp_nav_menu(array( 'theme_location' => 'primary_navigation' )); ?>
	</nav>
</div>