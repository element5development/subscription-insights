<?php 
/*----------------------------------------------------------------*\

	ARTICLE SECTION
	displaying content with background image

\*----------------------------------------------------------------*/
?>
<?php $image = get_sub_field('background'); ?>
<section id="section-<?php echo $template_args['sectionId']; ?>" class="cover <?php the_sub_field('width'); ?> <?php if( !$image ) :?>has-no-image<?php endif; ?> lazyload blur-up" data-expand="-150" data-bgset="<?php echo $image['sizes']['small']; ?> 350w, <?php echo $image['sizes']['medium']; ?> 700w, <?php echo $image['sizes']['large']; ?> 1000w, <?php echo $image['sizes']['xlarge']; ?> 1200w" data-sizes="auto">
	<div>
		<?php the_sub_field('content'); ?>
	</div>
</section>
