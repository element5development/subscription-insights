<?php 
/*----------------------------------------------------------------*\

	POST FOOTER
	Display copyright and navigation

\*----------------------------------------------------------------*/
?>

<footer class="post-footer">
	<div class="copyright">
		<p>Copyright <?php echo date('Y'); ?> <?php echo get_bloginfo( 'name' ); ?>. All Rights Reserved. Created by <a target="_blank" href="https://element5digital.com" rel="nofollow">Element5 Digital</a></p>
	</div>
</footer>