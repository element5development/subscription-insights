<?php 
/*----------------------------------------------------------------*\

	POST HEADER
	Display the post title

\*----------------------------------------------------------------*/
?>

<header class="post-head">
	<div class="is-extra-wide">
		<h1><?php the_title(); ?></h1>
	</div>
</header>